<?php

return array(
	'whitelist' => array(),
	'prefix' => 'WordPressDeps',
	'source' => getcwd() . '/vendor-source/',
	'destination' => getcwd() . '/vendor-scoped/',
);
